.. NeuralMag documentation master file, created by
   sphinx-quickstart on Wed Jan 10 17:29:15 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NeuralMag's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   getting_started
   nodal_fd
   field_terms

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
