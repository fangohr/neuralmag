.. module:: neuralmag

Field Terms
===========

Hallo

.. autoclass:: BulkDMIField
   :members:

.. autoclass:: DemagField
   :members:

.. autoclass:: ExchangeField
   :members:

.. autoclass:: InterfaceDMIField
   :members:

.. autoclass:: InterlayerExchangeField
   :members:

.. autoclass:: UniaxialAnisotropyField
   :members:
